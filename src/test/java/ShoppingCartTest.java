import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ShoppingCartTest  {
    ShoppingCart cart = new ShoppingCart();
    LineItem item1 = new LineItem(new Item("id-1", "pomme", ItemTypeEnum.FRUITS, 4),1);
    LineItem item2 = new LineItem(new Item("id-2", "coca", ItemTypeEnum.BOISSONS, 2),1);

    @Test
    public void add_lots_of_item() {
        //ajout de plusieurs produits au panier:

        cart.addItem(item1);
        cart.addItem(item2);

        int totaleItem = cart.totaleItemInCart();

        Assertions.assertEquals(totaleItem,2);
    }

    @Test
    public void add_item_in_cart() {


        //ajout au panier:
        cart.addItem(item1);

        //totale produit dans le panier:
        int totaleItem = cart.totaleItemInCart();

        //test:
        Assertions.assertEquals(totaleItem,1);

    }

    @Test
    public void add_lots_of_same_items_in_the_cart() {

        cart.addItem(item1);

        int totaleItem = cart.totaleItemInCart();
        Assertions.assertEquals(totaleItem,3);
    }

    @Test
    public void remove_item_from_cart() {
        cart.addItem(item1);
        cart.addItem(item2);

        cart.removeItem(item1);

        int totaleItem = cart.totaleItemInCart();

        Assertions.assertEquals(totaleItem,1);


    }

    @Test
    void get_item_in() {
    }
}