import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<LineItem> itemsInCart = new ArrayList<>();
    public void addItem(LineItem item) {
        this.itemsInCart.add(item);
    }

    public int totaleItemInCart() {
        int totaleItems = 0;
        for (LineItem item:itemsInCart) {
            totaleItems += item.getQuantity();
        }
        return totaleItems;
    }

    public void removeItem(LineItem item) {
        this.itemsInCart.remove(item);
    }
    // afficher le panier:
    public List<LineItem> getItems (){
        return itemsInCart;
    }
     // afficher le prix total

    public int totaleQuantite(){
        int totaleQuantite = 0;
       for(int i = 0; i < itemsInCart.size(); i ++){

           totaleQuantite = totaleQuantite + itemsInCart.get(i).getQuantity();
       }

        return totaleQuantite;
    }

    public int getFinalPrice(){
        int finalPrice = 0;
        for (LineItem lineItem : itemsInCart) {
            finalPrice = finalPrice + lineItem.getPriceLineItem();

        }
        return  finalPrice;
    }
}
