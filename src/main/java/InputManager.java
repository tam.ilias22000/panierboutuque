import java.util.Scanner;

public class InputManager {

    private static final Scanner sc = new Scanner(System.in);

    private static final InputManager ISTANCE = new InputManager();

    public static InputManager getInstance(){
        return ISTANCE;
    }

    private InputManager(){

    }

    public int getCostumerChoice() {

        Integer choice = null;
        do{
            try{
                choice = Integer.parseInt(sc.nextLine());
            }catch (Exception e){
                System.out.println("choix invalide");
            }

        }while (choice == null);
        return choice;
    }
}
