public class RemoveItem implements View {
    @Override
    public View render() {
        ShoppingCart cart = CustomerContext.getInstance().getCart();
        System.out.println(" Quel produit souhaitez-vous supprimer? ");

        for(int i = 0; i < cart.getItems().size(); i++){
            LineItem item = cart.getItems().get(i);

            int avaibleChoice = i +1;
            System.out.println(avaibleChoice + ")" + item.getItem().getName() + " au prix de : " + item.getItem().getPrice());
        }
        System.out.println(" quel produit souhaitez vous supprimer?");

        int choice = InputManager.getInstance().getCostumerChoice();
        if (choice >= 1 && choice <= cart.getItems().size()){
            cart.removeItem(cart.getItems().get(choice - 1));
            return new MenuShopping();
        }else {
            System.out.println("choix invalid");
            return this;
        }
    }
}
