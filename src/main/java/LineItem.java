import java.util.Objects;

public class LineItem {
    private Item item;
    private int quantity;

    public LineItem(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public LineItem(Item item) {
        this(item,1);
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPriceLineItem() {
        return (item.getPrice() * getQuantity());
    }

    @Override
    public String toString() {
        return "LineItem{" +
                "item=" + item +
                ", quantity=" + quantity +
                '}';
    }
}
