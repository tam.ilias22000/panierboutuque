public class DisplayShoppingCartScreen implements View {
    @Override
    public View render() {
        ShoppingCart cart = CustomerContext.getInstance().getCart();

        System.out.println(" voici votre commande : ");
        for (int i = 0;i<cart.getItems().size();i++){
            LineItem item = cart.getItems().get(i);
            int avaibleChoice = i + 1;
            System.out.println(avaibleChoice + ") "
                    +item.getItem().getName() + " au prix de : "
                    + item.getItem().getPrice()
                    + " a la quantite de : "
                    + item.getQuantity());

        }
        System.out.println("________________________________");
        System.out.println("le prix total est de:"
                + cart.getFinalPrice() +" une quantité de: "
                + cart.totaleQuantite());
        System.out.println("__________________________________");
        System.out.println("prix de tous vos produit:" + cart.getFinalPrice());

        return new MenuShopping();
    }
}
