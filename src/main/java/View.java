public interface View {
    static void loopOfScreens(View firstView){
        // l'ecran en cours = le prmier ecran:
        View displayView = firstView;

        // quand l'ecran actuel n'est pas null il faut le refaire rejouer
        while(displayView != null){
            displayView = displayView.render();
            // quand l'ecran n'est pas null il faut laisser un espace avec le prochain
            if( displayView != null){
                System.out.println("""
                    
                    
                    
                    -------               
                    """);
            }

        }
    }

    View render();
}
