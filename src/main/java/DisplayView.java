public class DisplayView implements View {
    @Override
    public View render() {

        System.out.println("""
                -------------------------
                VEUILLEZ FAIRE UN CHOIX:
                1) AJIUTER UN PRODUIT
                2) RETIRER UN PRODUIT
                3) VALIDER MON PANIER
                4) QUITTER L'APPLICATION
                -------------------------
                """);

        System.out.println(" que souhaitez-vous faire?");

        int choice = InputManager.getInstance().getCostumerChoice();

        return switch (choice){
            case 1 -> new AddItem();
            case 2 -> new RemoveItem();
            case 3 -> new DisplayShoppingCartScreen();
            //case 4 -> new GoodByeScreen();
            default -> this;
        };
    }
}
