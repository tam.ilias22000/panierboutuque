import java.util.ArrayList;
import java.util.List;

public class AddItem implements View {

    private static final List<LineItem> avaibleItems = List.of(
            new LineItem(new Item("1","banane",ItemTypeEnum.FRUITS,1),1),
            new LineItem(new Item("2","poire",ItemTypeEnum.FRUITS,2),1),
            new LineItem(new Item("3","pomme",ItemTypeEnum.FRUITS,4),1)
    );

    @Override
    public View render() {

        ShoppingCart cart = CustomerContext.getInstance().getCart();
        System.out.println(" AJOUT D'UN PRODUIT: ");
        for (int i = 0; i < avaibleItems.size(); i++) {
            LineItem item = avaibleItems.get(i);
            int avaibleChoice = i + 1;
            System.out.println(avaibleChoice + ")"
                    + item.getItem().getName() + " au prix de : "
                    + item.getItem().getPrice()
                    + " - une quantité de: "
                    + item.getQuantity());
        }
        int choice = InputManager.getInstance().getCostumerChoice();
        if (choice >= 1 && choice < avaibleItems.size()) {
            cart.addItem(avaibleItems.get(choice - 1));
            return new MenuShopping();
        } else {
            System.out.println("choix invalide");
            return this;
        }

    }
}
