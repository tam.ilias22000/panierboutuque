public class CustomerContext {

    private static final CustomerContext INSTANCE = new CustomerContext();

    public static CustomerContext getInstance(){ return INSTANCE;}

    private final ShoppingCart cart = new ShoppingCart();

    private CustomerContext(){

    }
    public ShoppingCart getCart(){ return cart;}


}
