public class Item {

    private final String id;
    private final String name;
    private final ItemTypeEnum itemType;
    private final int price;

    public Item(String id, String name, ItemTypeEnum itemType, int price) {
        this.id = id;
        if(name == "" || name.equals("")){
            throw new IllegalArgumentException("veuillez renseigner le nom du produit");
        }
        this.name = name;
        this.itemType = itemType;
        if(price < 0){
            throw new IllegalArgumentException("le prix ne peut etre negatif");
        }
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", itemType=" + itemType +
                ", price=" + price +
                '}';
    }
}
